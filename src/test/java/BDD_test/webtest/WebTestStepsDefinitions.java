package BDD_test.webtest;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebTestStepsDefinitions{

    WebDriver driver;
    WebDriverWait wait;
    String firstBlogTitle, firstCastTitle;

    @Before
    public void setup(){
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);
    }

    @Given("I go to devTo main page")
    public void i_go_to_devto_main_page() {
        driver.get("https://dev.to/");
    }
    @When("I click on first blog displayed")
    public void i_click_on_first_blog_displayed() {
       //driver.findElement(By.className("crayons-story__body")).click();
        WebElement firstBlog = driver.findElement(By.cssSelector("h2.crayons-story__title > a"));
        firstBlogTitle = firstBlog.getText();
        firstBlog.click();
    }

    @When("I go on Podcasts section")
    public void i_go_on_podcasts_section() {
        WebElement podcastSection = driver.findElement(By.linkText("Podcasts"));
        podcastSection.click();
    }
    @When("I click on first Podcast")
    public void i_click_on_first_podcast() {
        wait.until(ExpectedConditions.titleContains("Podcasts"));
        WebElement firstCast = driver.findElement(By.tagName("h3"));
        firstCastTitle = firstCast.getText();
        firstCastTitle = firstCastTitle.replace("podcast", "");
        firstCast.click();
    }

    @Then("I should be redirected to blog page")
    public void i_should_be_redirected_to_blog_page() {
        wait.until(ExpectedConditions.titleContains(firstBlogTitle));
        WebElement blogTitle = driver.findElement(By.tagName("h1"));
        String blogTitleText = blogTitle.getText();
        System.out.println("to jest tex mojego bloga : " + blogTitleText);
        Assert.assertEquals(firstBlogTitle, blogTitleText);
    }
    @Then("I should be redirected to Podcast page")
    public void i_should_be_redirected_to_podcast_page() {
        wait.until(ExpectedConditions.titleContains(firstCastTitle));
        WebElement castTitle = driver.findElement(By.tagName("h1"));
        String castTitleText = castTitle.getText();
        System.out.println("to jest tex mojego castu : " + castTitleText);
        Assert.assertEquals(firstCastTitle, castTitleText);
    }

}
/**/
